json.array!(@chats) do |chat|
  json.extract! chat, :id, :session_name, :ativo
  json.url chat_url(chat, format: :json)
end
