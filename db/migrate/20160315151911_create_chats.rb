class CreateChats < ActiveRecord::Migration
  def change
    create_table :chats do |t|
      t.string :session_name
      t.boolean :ativo

      t.timestamps null: false
    end
    add_index :chats, :ativo
  end
end
